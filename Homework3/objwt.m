% define objective funtion (without a derivative supplied)

function llh=objwt(beta)
global y X
 llh=sum(-exp(X*beta)+y.*(X*beta));
end
