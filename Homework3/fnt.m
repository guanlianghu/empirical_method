%% Homework Q#1 newton method - Jacobian and function value
function [f1,f2]=fnt(x,y)
    f1= log(x)-psi(x)-log(y);
    f2= 1/x-trigamma(x);
end