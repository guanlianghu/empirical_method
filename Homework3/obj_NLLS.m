function objf=obj_NLLS(bta)
global y X
bx=X*bta;
objf=sum((y-exp(bx)).^2);
end