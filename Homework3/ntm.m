%% Homework Q#1 newton method
function sita1=ntm(x0,y0)
    x=x0;
    y=y0;
    tol=10e-8;
    maxit=10000;
    for it=1:maxit  
        [fval,fjac] = fnt(x,y);
        x = x - fjac\fval;
    
        if norm(fval) < tol
            sita1=x;
            break
        end
    end
end