% define objective funtion (with a derivative supplied)
function dllh2=objw(beta)
global y X
 ebx=exp(X*beta);
 dllh2=sum((-ebx+y)*ones(1,6).*X)*sum((-ebx+y)*ones(1,6).*X)';

end
