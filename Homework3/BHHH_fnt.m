function [g,H] = BHHH_fnt(bta00,y1,X1)

ebx=exp(X1*bta00);
s=(-ebx+y1)*ones(1,6).*X1;
g=sum(s)';
H=s'*s;

end