function [B,Cov]=nlls(y1,X1)
bta0        =zeros(6,1);
n=length(y1);
B=fminunc(@(bta) obj_NLLS(bta),bta0); 
bx=X1*B;
ebx=exp(bx);
V=(1/n)*((-ebx)*ones(1,6).*X1)'*((-ebx)*ones(1,6).*X1);
H=(1/n)*((-ebx)*ones(1,6).*X1)'*((-ebx)*ones(1,6).*X1)+...
    (1/n)*((y1-ebx).*(-ebx)*ones(1,6).*X1)'*(ebx*ones(1,6).*X1);
Cov=inv(H)*V*inv(H)/n;
end