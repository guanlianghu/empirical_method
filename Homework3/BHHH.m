function [B,Cov]=BHHH(bta00,y1,X1)
    btai=bta00;
    tol=10e-5;
    maxit=10000;
    for it=1:maxit  
        [g,H] = BHHH_fnt(btai,y1,X1);
        
        bta = btai + H\g;  
        
        if norm(bta-btai) < tol
            B=bta;
            break
        end
        btai=bta; 
    end
    Cov=inv(H);  
    it
end