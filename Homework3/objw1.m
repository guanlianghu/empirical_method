% define objective funtion (with a derivative supplied)
function [f, g]=objw1(beta)
global y X

f=-sum(-exp(X*beta)+y.*(X*beta));

if nargout > 1
ebx=exp(X*beta);
g=-sum((-ebx+y)*ones(1,6).*X)';

end
