%% Homework #3
% by Guanliang Hu, Oct 18, 2017
clear;
clc
% addpath(CEtools/)
%% Question #1 (a)
% please check pdf
%% Question #1 (c) 
% please check pdf
%% Question #1 (c)    
i=0;
for y=1.1 :0.1: 3
    i=i+1;
    sita1(i)=ntm(1,y);    
end

% this procedure does not seem to return estimate of the paraemtes, 
% I don't understand what it does.

% since we don't know y1, we are unable to calculate the sita2.
% assume we know y1, then sita2=sita1./y1;

% the method to get the covariance matrix is similar as the one in the 
% case of BHHH in the Q#2 

%% Question #1 (d)
y=1.1 :0.1: 3;
plot(y, sita1)

%% Question #2
clear;
clc
load('hw3.mat')
global y X
    
% initial guess

bta0        =zeros(6,1);

%% Question #2 (a)
    %% Algorithm #1 :FMINUNC without a derivative supplied
     [bta1,FVAL1] = fminunc(@(bta) nobjwt(bta),bta0) 
    %% Algorithm #2 :FMINUNC with a derivative supplied
    options = optimoptions('fminunc','Algorithm','trust-region','SpecifyObjectiveGradient',true);
    %options = optimoptions('fminunc','Algorithm','trust-region');
    problem.options = options;
    problem.x0 = bta0;
    problem.objective = @objw1;
    problem.solver = 'fminunc';
    bta2= fminunc(problem)
    
    

    %% Algorithm #3 :Nelder Mead
    % if using [0 0 0 0 0 0] as intital guess
      optset('neldmead','maxit',10000); 
     [bta3,S] = neldmead('objwt',bta0)

    %% Algorithm #4 :and the BHHH
    % if using [0 0 0 0 0 0] as intital guess
     [bta4,BHHH_cov]  = BHHH(bta0,y,X)
      BHHH_bta=bta4;
     for i=1 : 6
        BHHH_std(i)= BHHH_cov(i,i)^.5;
     end
%% Question #2 (b)
%Report the eigenvalues for the Hessian approximation for the BHHH MLE method from the last question.
    E1 = eig(-BHHH_cov) 
%Report the eigenvalues for the initial Hessian, and the Hessian at the estimated parameters.   
    bx=X*BHHH_bta;
    ebx=exp(bx);
    n=length(y);
    Hessian1=(1/n)*((-ebx)*ones(1,6).*X/n)'*X/n;

    E2 = eig(Hessian1)
%% Question #2 (c)  
    [NLLS_bta, NLLS_cov]=nlls(y,X)
    for i=1 : 6
    NLLS_std(i)= NLLS_cov(i,i)^.5;
    end

%% Question #2 (d)

    disp('the standard errors for the BHHH is:'), BHHH_std
    disp('the standard errors for the NLLS is:'), NLLS_std
    
    
    

