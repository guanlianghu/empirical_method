% define objective funtion (without a derivative supplied)
function llh=nobjwt(beta)
global y X
 llh=-sum(-exp(X*beta)+y.*(X*beta));
end
