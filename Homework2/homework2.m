%% Homewokr #2-Discrete Choice Demand Pricing Equilibrium
clc;
clear;

% intial value
P0=[1,1,1
    0,0,0
    0,1,2
    3,2,1];

%% Question #1

% simple algebra
v=[-1,-1,-1]';
p=[1,1,1]';
exp_v_p=exp(v-p);

q=exp_v_p./(1+sum(exp_v_p));

Q1_q=[q 
    1-sum(q)];
disp('----------Question#1:-----------');
disp('the demand for each option:');
disp(Q1_q);
disp('===========================');
%% Question #2

% addpath('\CEtools');
% Set options for Broyden
optset('broyden','showiters',1000);
optset('broyden','maxit',300) ;
optset('broyden','tol',1e-8) ;

Q2_P=zeros(4,3);

tic
for i=1:4
    % initial guess 1
    P=P0(i,:)';

    % *Call Broyden*
    [P,pfval]  = broyden(@(P) pval(P,v),P); 
    % here it's good to also extract the number of iterations
    Q2_P(i,:)=P';
    
    
    disp('----------Question#2:-----------');
    disp('Inital value:');
    disp(P0(i,:));
    disp('the Nash pricing equilibrium:')
    disp(Q2_P(i,:));
    disp('--------------------------------');

end
toc
    disp('convergence criteria:');

    A='maxmum interation:300';
    B='tolerance:1e-8';
    disp(A);
    disp(B);
    disp('==========================='); 



%% Question #3
% Guass-Jacobi method (using the secant method for each sub-iteration)

tol=1e-18;
% this tolerance is an overshoot. it may be higher than accumulated error
% in the computation process.
maxit=130000;
Q3_P=zeros(4,3);
delta=0.00000000001;
tic
for i=1:4
    it=0;
    Pnew=P0(i,:)';
    Pold=0.9*Pnew+0.0001;
    
    while it<=maxit &&  tol*abs(1+max(Pold))<=max(abs(Pnew-Pold))
            
            Pold=Pnew;
            p1=Pold(1);
            p2=Pold(2);
            p3=Pold(3);
            
            for j=1:3
                
            iti(j)=0;
            p_new(j)=p1;
            p_old(j)=p1+1;
            while iti(j)<=maxit &&  tol*abs(1+p_old(j))<=abs(p_new(j)-p_old(j))
                
                p_old(j)=p_new(j);
                
                ppold=[p1 p2 p3]';
                ppold(j)=p_old(j);
                
                ppnew=ppold;
                ppnew(j)=ppnew(j)+delta;
                
                pv=pval(ppnew,v);
                pv1=pval(ppold,v);
                
                delta_f1=(pv(j)-pv1(j))/delta;                    
                p_new(j)=p_old(j)-1./delta_f1*pv1(j);
  
            iti(j)=iti(j)+1;    
            end
            end
            
                 

        it=it+1; 
        Pnew=p_new';
    end
    
    Q3_P(i,:)=Pnew';
    disp('----------Question#3:-----------');
    disp('Inital value:');
    disp(P0(i,:));
    disp('Iteration time:');
    disp(it);
    disp('the Nash pricing equilibrium:')
    disp(Pnew');
    disp('--------------------------------');

end
toc
    disp('The Guass-Jacobi method (using the secant method for each sub-iteration) is slower than Broyden!')
    disp('==========================='); 


%% Question #4
% simple iteration
tol=1e-8;
maxit=300;
Q4_P=zeros(4,3);
tic
for i=1:4
    it=0;
    Pold=P0(i,:);
    Pnew=1000+P0(i,:);
    while it<=maxit &&  tol*abs(1+max(Pold))<=max(abs(Pnew-Pold))
        Pold=Pnew;
        exp_v_p=exp(v'-Pold);
        q=exp_v_p./(1+sum(exp_v_p));
        Pnew=1./(1-q);
        it=it+1;
    end
    
    Q4_P(i,:)=Pnew;
    disp('----------Question#4:-----------');
    disp('Inital value:');
    disp(P0(i,:));
    disp('Iteration time:');
    disp(it);
    disp('the Nash pricing equilibrium:')
    disp(Pnew);
    disp('--------------------------------');
    disp('It converges!')
 
end
toc
    disp('It is faster than Broyden!')
    disp('===========================');    

%% Question #5
% Gauss-Jacobi, but take only a single secant step for each equaiton

tol=1e-8;
maxit=300;
Q3_P=zeros(4,3);
tic
for i=1:4
    it=0;
    Pold=P0(i,:)';
    Pnew=0.9*Pold+0.0001;
    while it<=maxit &&  tol*abs(1+max(Pold))<=max(abs(Pnew-Pold))
        
        delta_f=(pval(Pnew,v)-pval(Pold,v))./(Pnew-Pold);
        Pold=Pnew;       
        Pnew=Pold-1./delta_f.*pval(Pold,v);
        it=it+1; 
    end
    
    Q3_P(i,:)=Pnew';
    disp('----------Question#5:-----------');
    disp('Inital value:');
    disp(P0(i,:));
    disp('Iteration time:');
    disp(it);
    disp('the Nash pricing equilibrium:')
    disp(Pnew');
    disp('--------------------------------');

end
toc
    disp('The Guass-Jacobi method (only a single secant step for each equaiton) is faster!')
    disp('==========================='); 



%% END OF HOMEWORK #2
