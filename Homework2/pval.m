function pfval=pval(p,v)

    exp_v_p=exp(v-p);
    q=exp_v_p./(1+sum(exp_v_p));
    pfval   = 1+p.*q-p;
