%% Homework #1 
clc
clear all
%% Problem # 1


X   = [1, 1.5, 3, 4, 5, 7, 9, 10];                  % generate X
Y1  = -2+0.5*X;                                     % calculate Y1 
Y2  = -2+0.5*X.^2;                                  % calculate Y2

% plot Y1 and Y2
figure;
plot (X,Y1);
hold on
plot (X,Y2);
title ('Y1 and Y2')
xlabel('X')
ylabel('Y1 or Y2')
legend('Y1','Y2')
hold off
% end

%% Problem #2

X      = linspace(-10,20,200);
sumX    = sum(X)
%end

%% Problem #3

A   = [2 4 6; 1 7 5; 3 12 4;]
B   = [-2; 3; 10]
C   = A'*B
D   = (A'*A)^-1*B
i3  = ones(1,3)
E   = sum(sum(A.*kron(i3,B)))
F   = A; F(2,:)=[]; F(:,3)=[]
X  = linsolve(A,B)

%end

%% Problem #4

i5  = eye(5);
B  = kron(i5,A)

%end

%% Problem #5

rng('default');
A   =randn(5,3)*5+10
A1  =A>=10

% end

%% Problem #6

filename    = 'datahw1.csv'
M           = csvread(filename);                            % import data
nobs        = size(M,1);
Cons        = ones(nobs,1);        
X           = M; 
X(:,1)      = Cons;
X(:,2)      = [];
X(:,4)      = [];
nvar        = size(X,2);
Y           = M(:,5);
XX          = X'*X;

inv_XX      = inv(XX);
% inv is a slow operation. (X'*X)\X'*Y would have been faster
beta        = inv_XX*(X'*Y)                                 % estimate the coef.
resid       = Y-X*beta;                                     % residual
X_res       = X .* resid(:,ones(1,4));
Robust_CoV  = (nobs/(nobs-nvar))*inv_XX*X_res'*X_res*inv_XX % robust covariance matrix
Robust_SE   = sqrt(diag(Robust_CoV))                        % robust strandard error
t_stat      = beta./Robust_SE                               % t-statistics


% P-value
try
    p_val   = (1 - tcdf(abs(t_stat),nobs-nvar-1)) * 2
catch
    p_val   = (1 - 0.5 * erfc(-0.7071 * abs(t_stat))) * 2
end

% end


%% end of Homework#1









