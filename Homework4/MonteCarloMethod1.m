function lnL=MonteCarloMethod1(Para)
global N T X Y Z
global randBta_u randBta_l
Nodes = 100;

bta0 = Para(1);
sigmabta2= Para(2);
gamma =Para(3);

Li_u = ones(N,Nodes);
Li_l = ones(N,Nodes);

for iN = 1: N
    for iNodes = 1: Nodes
        for iT=1:T
            epi=-log(randBta_u(iN,iNodes))*X(iT,iN)+gamma*Z(iT,iN);
            t1 = exp(-epi);
            %t1 = exp(log(randBta_u(iN,iNodes))*X(iT,iN)+gamma*Z(iT,iN));
            Li_u(iN,iNodes) = Li_u(iN,iNodes)*(1/(1+t1))^Y(iT,iN)*...
                (t1/(1+t1))^(1-Y(iT,iN));
            
            epi=log(randBta_l(iN,iNodes))*X(iT,iN)+gamma*Z(iT,iN);
            t2 = exp(-epi);
            %t2 = exp(-log(randBta_l(iN,iNodes))*X(iT,iN)+gamma*Z(iT,iN));
            Li_l(iN,iNodes) = Li_l(iN,iNodes)*(1/(1+t2))^Y(iT,iN)*...
                (t2/(1+t2))^(1-Y(iT,iN));
            
        end
        Li_u(iN,iNodes) = Li_u(iN,iNodes)*(1/(2*pi*sigmabta2)^0.5)*exp(-(-log(randBta_u(iN,iNodes))-bta0)^2/2/sigmabta2)/randBta_u(iN,iNodes);
        Li_l(iN,iNodes) = Li_l(iN,iNodes)*(1/(2*pi*sigmabta2)^0.5)*exp(-(log(randBta_l(iN,iNodes))-bta0)^2/2/sigmabta2)/(randBta_l(iN,iNodes));
    end
end

Intr_u = sum(Li_u,2)/Nodes;
Intr_l = sum(Li_l,2)/Nodes;
lnIntr   = log(Intr_u+Intr_l);
lnL = -sum(lnIntr);
%fprintf('The likelihood is: %d when using Monte Carlo Method.\n', L);
end
