function lnL=MonteCarloMethod3(Para)
% this is the case without transforming the interval
global N T X Y Z
global randbtamu0
Nodes = 1000;
bta0 = Para(1);
sigmabta2 = Para(2);
gamma = Para(3);
mu0 = Para(4);
sigmamu2 = Para(5);
sigmabtamu = Para(6);

% desired correlation
M =[sigmabta2^0.5  sigmabtamu
    sigmabtamu  sigmamu2^0.5  ];
% induce correlation, check correlations
C = chol(M);
randbtamu = randbtamu0 * C;
randbtamu(:,1) = bta0+randbtamu(:,1);
randbtamu(:,2) = mu0+randbtamu(:,2);

randbta=reshape(randbtamu(:,1),[N Nodes]);
randmu=reshape(randbtamu(:,2),[N Nodes]);

Li = ones(N,Nodes);


for iN = 1: N
    for iNodes = 1: Nodes
        for iT=1:T
            epi= randbta(iN,iNodes)*X(iT,iN)+gamma*Z(iT,iN)+randmu(iN,iNodes);
            t1 = exp(-epi);
            Li(iN,iNodes) = Li(iN,iNodes)*(1/(1+t1))^Y(iT,iN)*...
                (t1/(1+t1))^(1-Y(iT,iN));
        end
    end
end

lnIntr = log(sum(Li,2)/Nodes);
lnL    = -sum(lnIntr);

end
