function lnL=MonteCarloMethod2(Para)
% this is the case without transforming the interval
global N T X Y Z
global randBta0
Nodes = 100;
bta0 = Para(1);
sigmabta2= Para(2);
gamma =Para(3);
Li = ones(N,Nodes);
randBta=bta0+sigmabta2^0.5*randBta0;

for iN = 1: N
    for iNodes = 1: Nodes
        for iT=1:T
            epi= randBta(iN,iNodes)*X(iT,iN)+gamma*Z(iT,iN);
            t1 = exp(-epi);
            Li(iN,iNodes) = Li(iN,iNodes)*(1/(1+t1))^Y(iT,iN)*...
                (t1/(1+t1))^(1-Y(iT,iN));
        end
        %Li(iN,iNodes) = Li(iN,iNodes)*(1/(2*pi*sigmabta2)^0.5)*exp(-(randBta(iN,iNodes)-bta0)^2/2/sigmabta2);
    end
end

lnIntr = log(sum(Li,2)/Nodes);
lnL    = -sum(lnIntr);

end
